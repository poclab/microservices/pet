# pet

pet Services

[![Docker Repository on Quay](https://quay.io/repository/sidlors/pet/status?token=1f107f4e-a826-4359-b455-3a6abde5a010 "Docker Repository on Quay")](https://quay.io/repository/sidlors/pet)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=sidlors-lab-pet&metric=alert_status)](https://sonarcloud.io/dashboard?id=sidlors-lab-pet)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=sidlors-lab-pet&metric=bugs)](https://sonarcloud.io/dashboard?id=sidlors-lab-pet)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=sidlors-lab-pet&metric=coverage)](https://sonarcloud.io/dashboard?id=sidlors-lab-pet)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=sidlors-lab-pet&metric=ncloc)](https://sonarcloud.io/dashboard?id=sidlors-lab-pet)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=sidlors-lab-pet&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=sidlors-lab-pet)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=sidlors-lab-pet&metric=security_rating)](https://sonarcloud.io/dashboard?id=sidlors-lab-pet)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=sidlors-lab-pet&metric=sqale_index)](https://sonarcloud.io/dashboard?id=sidlors-lab-pet)
