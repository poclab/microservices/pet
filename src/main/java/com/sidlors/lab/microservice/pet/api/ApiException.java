package com.sidlors.lab.microservice.pet.api;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-06-21T19:35:07.921Z")

public class ApiException extends Exception{
    /**
	 * 
	 */
	private static final long serialVersionUID = -3085457542904250268L;
	private int code;
    public ApiException (int code, String msg) {
        super(msg);
        this.code = code;
    }
}
