package com.sidlors.lab.microservice.pet.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sidlors.lab.microservice.pet.model.Pet;
import com.sidlors.lab.microservice.pet.service.PetService;

import io.swagger.annotations.ApiParam;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-06-21T19:35:07.921Z")

@Controller
@RequestMapping("/pet")
public class PetApiController implements PetApi {

    private ObjectMapper objectMapper;

    private HttpServletRequest request;
    
    @Autowired
    private PetService petService;

    @Autowired
    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Autowired
    public void setHttpServletRequest( HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public Optional<ObjectMapper> getObjectMapper() {
        return Optional.ofNullable(objectMapper);
    }

    @Override
    public Optional<HttpServletRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    @Override
    public Optional<String> getAcceptHeader() {
        return getRequest().map(r -> r.getHeader("Accept"));
    }
    
    public ResponseEntity<Pet> addPet(@ApiParam(value = "Pet object that needs to be added to the store" ,required=true )  @Valid @RequestBody Pet body) {
        if(getObjectMapper().isPresent() && getAcceptHeader().isPresent() && getAcceptHeader().get().contains("application/json")) {
        	Pet response = petService.addPet(body);
        	if(response!=null)
        		return new ResponseEntity<>(HttpStatus.ACCEPTED);	      
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default PetApi interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    
    public ResponseEntity<Pet> deletePet(@ApiParam(value = "Pet id to delete",required=true) @PathVariable("petId") Long petId){
    	if(getObjectMapper().isPresent() && getAcceptHeader().isPresent() && getAcceptHeader().get().contains("application/json")) {        	
	        Pet response = petService.getPetById(petId);	        		
	        if(response!=null) {
	        	petService.deletePet(petId);
	       		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	       	}
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default PetApi interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    
    
    public ResponseEntity<Pet> getPetById(@ApiParam(value = "ID of pet to return",required=true) @PathVariable("petId") Long petId){
    	if(getObjectMapper().isPresent() && getAcceptHeader().isPresent() && getAcceptHeader().get().contains("application/json")) {        	
	        try {
	        	Pet response = petService.getPetById(petId);	        		
	        	if(response!=null)
	        		return new ResponseEntity<>(getObjectMapper().get().readValue("{}",Pet.class), HttpStatus.ACCEPTED);	        		
	        } catch (IOException e) {
	        	log.error("Couldn't serialize response for content type application/json", e);	                
	        }        	
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default PetApi interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    
    public ResponseEntity<ArrayList<Pet>> getPetAll() throws ApiException{
    	if(getObjectMapper().isPresent() && getAcceptHeader().isPresent() && getAcceptHeader().get().contains("application/json")) {
        	try {
	        	List<Pet> pets = petService.getPetAll();	        		
	        	if(pets!=null)
	        		return new ResponseEntity<>(getObjectMapper().get().readValue("{}",ArrayList.class), HttpStatus.ACCEPTED);	        	
	        			        		
	        } catch (IOException e) {
	        	throw new ApiException(1, "ObjectMapper or HttpServletRequest not configured in default PetApi interface so no example is generated");
        	}
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default PetApi interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    
    public ResponseEntity<Pet> updatePet(@ApiParam(value = "Pet object that needs to be update to the store" ,required=true )  @Valid @RequestBody Pet body){
    	 if(getObjectMapper().isPresent() && getAcceptHeader().isPresent() && getAcceptHeader().get().contains("application/json")) {
         	Pet response = petService.updatePet(body);
 	        if(response!=null)
 	        	return new ResponseEntity<>(HttpStatus.ACCEPTED); 	        
         } else {
        	 log.error("ObjectMapper or HttpServletRequest not configured in default PetApi interface so no example is generated");             
         }
         return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    
    

}
