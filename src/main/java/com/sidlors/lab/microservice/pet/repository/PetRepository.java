package com.sidlors.lab.microservice.pet.repository;

import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sidlors.lab.microservice.pet.model.Pet;

@Repository
public interface PetRepository extends CrudRepository<Pet, Long> {

	Optional<Pet> findById(Long Id);
	
	
}
