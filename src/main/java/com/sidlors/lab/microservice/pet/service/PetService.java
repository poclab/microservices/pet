package com.sidlors.lab.microservice.pet.service;

import java.util.List;

import com.sidlors.lab.microservice.pet.model.Pet;




public interface PetService {

    public abstract Pet addPet(Pet body);
    public abstract Pet deletePet(Long idPet);
    public abstract List<Pet> getPetAll() ;
    public abstract Pet getPetById(Long petId) ;
    public abstract Pet updatePet(Pet body) ;

}
