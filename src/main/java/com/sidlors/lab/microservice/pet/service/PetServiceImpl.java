package com.sidlors.lab.microservice.pet.service;

import java.util.ArrayList;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.sidlors.lab.microservice.pet.model.Pet;
import com.sidlors.lab.microservice.pet.repository.PetRepository;

@Service
public class PetServiceImpl implements PetService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private PetRepository petRepository;

	@Override
	public Pet addPet(Pet body) {
		Pet response = petRepository.save(body);
		Assert.isNull(response, "pet not added: " + body.toString());
		
		return body; 
	}

	@Override
	public Pet deletePet(Long idPet) {
		petRepository.deleteById(idPet);
		Pet pet = new Pet();
		pet.setId(idPet);
		return pet;
	}

	@Override
	public Pet getPetById(Long petId) {
		Optional<Pet> pet = petRepository.findById(petId);
		Assert.isNull(pet, "can't find pet with id " + petId);
		return pet.orElse(null);
	}

	@Override
	public Pet updatePet(Pet body) {
		petRepository.deleteById(body.getId());
		Optional<Pet> existing = petRepository.findById(body.getId());
		Assert.isNull(existing, "pet already exists: " + body.getId());
		petRepository.save(body);
		return body;
	}
	
	@Override
	public ArrayList<Pet> getPetAll() {
		ArrayList<Pet> pets = (ArrayList<Pet>) petRepository.findAll();
		Assert.isNull(pets, "can't find pets");
		return pets;
	}


	

}
