package com.sidlors.lab.microservice.pet.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.sidlors.lab.microservice.pet.RFC3339DateFormat;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RFC3339DateFormat.class)
@WebAppConfiguration
public class RFC3339DateFormatTest {

    @InjectMocks
	private RFC3339DateFormat rfc3339DateFormat;


    @Test
    @DisplayName("Should FC3339Date Format is correct ")
    public void shouldRFC3339DateFormatisTrue(){
        Date date = new Date();
        RFC3339DateFormat dateFormat = new RFC3339DateFormat();
        assertEquals(rfc3339DateFormat.format(date),dateFormat.format(date));
    }

}
