package com.sidlors.lab.microservice.pet.test.controller;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.servlet.http.HttpServletRequest;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sidlors.lab.microservice.pet.PetApp;
import com.sidlors.lab.microservice.pet.api.PetApiController;
import com.sidlors.lab.microservice.pet.model.Category;
import com.sidlors.lab.microservice.pet.model.Pet;
import com.sidlors.lab.microservice.pet.model.Tag;
import com.sidlors.lab.microservice.pet.service.PetService;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = PetApp.class)
@WebAppConfiguration
public class PetApiControllerTest {

	private static final ObjectMapper mapper = new ObjectMapper();
	
	private static final Logger log = LoggerFactory.getLogger(PetApiControllerTest.class);


	@InjectMocks
	private PetApiController PetApiController;

	@Mock
	private PetService petService;

	@Mock
	private  ObjectMapper objectMapper;

	@Mock
    private  HttpServletRequest request;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(PetApiController).build();
	}

	@Test
	public void shouldAddPet() throws Exception {
		
		Pet pet = new Pet();
		pet.setId(1L);
		pet.setName("Andres");
		pet.setPhotoUrls(new ArrayList<String>());		
		pet.setCategory(new Category());
		pet.getCategory().setId(1L);
		pet.getCategory().setName("Perra");

		String json = mapper.writeValueAsString(pet);
		when(request.getHeader("Accept")).thenReturn("application/json");
		when(petService.addPet(pet)).thenReturn(pet);
		mockMvc.perform(post("/pet/pet")
			.contentType(MediaType.APPLICATION_JSON)
			.header("Content-Type", MediaType.APPLICATION_JSON)
			.content(json))
			.andExpect(status().isAccepted());
	}
	
	@Test
	public void shouldNotAddPet() throws Exception {
		
		Pet pet = new Pet();
		pet.setName("");
		pet.setPhotoUrls(new ArrayList<String>());		
		pet.setCategory(new Category());
		pet.getCategory().setId(1L);
		pet.getCategory().setName("");
		
		String json = mapper.writeValueAsString(pet);
		mockMvc.perform(post("/pet/pet")
			.contentType(MediaType.APPLICATION_JSON)
			.header("Content-Type", MediaType.APPLICATION_JSON)
			.content(json))
			.andExpect(status().isBadRequest());
	}
	
	@Test
	public void shouldGetPetById() throws Exception {
		
		Pet pet = new Pet();
		pet.setId(1L);

		when(request.getHeader("Accept")).thenReturn("application/json");
		when(petService.getPetById(pet.getId())).thenReturn(pet);
		mockMvc.perform(get("/pet/pet/"+pet.getId())
			.contentType(MediaType.APPLICATION_JSON)
			.header("Content-Type", MediaType.APPLICATION_JSON)
			.content(""))
			.andExpect(status().isAccepted());
		
	}
	
	@Test
	public void shouldNotGetPetById() throws Exception {
		
		Pet pet = new Pet();
		pet.setId(1L);

		mockMvc.perform(get("/pet/pet/"+pet.getId())
			.contentType(MediaType.APPLICATION_JSON)
			.header("Content-Type", MediaType.APPLICATION_JSON)
			.content(""))
			.andExpect(status().isBadRequest());
	}
	
	@Test
	public void shouldDeletePet() throws Exception {
		
		Pet pet = new Pet();
		pet.setId(1L);

		String json = mapper.writeValueAsString(pet);
		when(request.getHeader("Accept")).thenReturn("application/json");
		when(petService.deletePet(pet.getId())).thenReturn(pet);
		mockMvc.perform(MockMvcRequestBuilders.delete("/pet/pet/"+pet.getId())
			.contentType(MediaType.APPLICATION_JSON)
			.header("Content-Type", MediaType.APPLICATION_JSON)
			.content(json))
			.andExpect(status().isBadRequest());
	}
	
	@Test
	public void shouldNotDeletePet() throws Exception {
		
		Pet pet = new Pet();
		pet.setId(1L);

		String json = mapper.writeValueAsString(pet);
		mockMvc.perform(MockMvcRequestBuilders.delete("/pet/pet/"+pet.getId())
			.contentType(MediaType.APPLICATION_JSON)
			.header("Content-Type", MediaType.APPLICATION_JSON)
			.content(json))
			.andExpect(status().isBadRequest());
	}
	
	@Test
	public void shouldGetPetAll() throws Exception {
		
		ArrayList<Pet> pets = new ArrayList<Pet>();
		when(request.getHeader("Accept")).thenReturn("application/json");
		when(petService.getPetAll()).thenReturn(pets);
		mockMvc.perform(get("/pet/pet/")
			.contentType(MediaType.APPLICATION_JSON)
			.header("Content-Type", MediaType.APPLICATION_JSON))
			.andExpect(status().isAccepted());
	}
	
	@Test
	public void shouldNotGetPetAll() throws Exception {
		
		mockMvc.perform(get("/pet/pet/")
			.contentType(MediaType.APPLICATION_JSON)
			.header("Content-Type", MediaType.APPLICATION_JSON))
			.andExpect(status().isBadRequest());
	}

	@Test
	public void shouldUpdatePet() throws Exception {
		
		ArrayList<String> photos = new ArrayList<String>();
		ArrayList<Tag> tags = new ArrayList<Tag>();
		
		Tag tag = new Tag();
		tag.setId(1L);
		tag.setName("X");
		tags.add(tag);

		Pet pet = new Pet();
		pet.setId(1L);
		pet.setName("Andres");
		pet.setPhotoUrls(photos);		
		pet.setCategory(new Category());
		pet.getCategory().setId(1L);
		pet.getCategory().setName("Perra");

		String json = mapper.writeValueAsString(pet);
		when(request.getHeader("Accept")).thenReturn("application/json");
		when(petService.updatePet(pet)).thenReturn(pet);
		mockMvc.perform(post("/pet/pet/update/")
			.contentType(MediaType.APPLICATION_JSON)
			.header("Content-Type", MediaType.APPLICATION_JSON)
			.content(json))
			.andExpect(status().isAccepted());
	}
	
	@Test
	public void shouldNotUpdatePet() throws Exception {
		
		Pet pet = new Pet();
		pet.setName("");
		pet.setPhotoUrls(new ArrayList<String>());		
		pet.setCategory(new Category());
		pet.getCategory().setId(1L);
		pet.getCategory().setName("");

		String json = mapper.writeValueAsString(pet);
		
		mockMvc.perform(post("/pet/pet/update/")
			.contentType(MediaType.APPLICATION_JSON)
			.header("Content-Type", MediaType.APPLICATION_JSON)
			.content(json))
			.andExpect(status().isBadRequest());
	}
	
}
