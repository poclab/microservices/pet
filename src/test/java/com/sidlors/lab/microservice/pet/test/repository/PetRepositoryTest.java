package com.sidlors.lab.microservice.pet.test.repository;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sidlors.lab.microservice.pet.PetApp;
import com.sidlors.lab.microservice.pet.model.Pet;
import com.sidlors.lab.microservice.pet.repository.PetRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = PetApp.class)
@WebAppConfiguration
public class PetRepositoryTest {

	@Autowired
	private PetRepository petRepository;


	@Test
	public void shouldFindPetById() {

		Pet stub = getStubPet();
		petRepository.save(stub);

		Pet found = petRepository.findById(stub.getId()).orElse(null);
		assertEquals(stub.getId(), found.getId());
	}

	private Pet getStubPet() {
		
		Pet Pet = new Pet();
		Pet.setId(11L);

		
		return Pet;
	}
}
