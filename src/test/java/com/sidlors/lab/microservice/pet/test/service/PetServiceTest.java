package com.sidlors.lab.microservice.pet.test.service;


import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.util.Assert;

import com.sidlors.lab.microservice.pet.model.Category;
import com.sidlors.lab.microservice.pet.model.Pet;
import com.sidlors.lab.microservice.pet.repository.PetRepository;
import com.sidlors.lab.microservice.pet.service.PetServiceImpl;


public class PetServiceTest {

	@InjectMocks
	private PetServiceImpl petService;

	@Mock
	private PetRepository petRepository;
	

	@Before
	public void setup() {
		initMocks(this);
	}

	@Test
	public void shouldRegisterNewPet() throws Exception {
		ArrayList<String> photos = new ArrayList<String>();
		
		Pet pet = new Pet();
		pet.setId(1L);
		pet.setName("Andres");
		pet.setPhotoUrls(photos);		
		pet.setCategory(new Category());
		pet.getCategory().setId(1L);
		pet.getCategory().setName("Perra");
		
		Pet petNew=petService.addPet(pet);

		assertEquals(petNew.getId(), pet.getId());

		verify(petRepository, times(1)).save(pet);
	}
	
	@Test
	public void shouldDeletePet() throws Exception {
		Pet pet = new Pet();
		pet.setId(1L);
		
		Pet petNew=petService.deletePet(pet.getId());
		assertEquals(petNew.getId(), pet.getId());
	}
	
	@Ignore
	public void shouldGetPetById() throws Exception {
		Pet pet = new Pet();
		pet.setId(1L);
		
		Pet findPet = petService.getPetById(pet.getId());
		Assert.isNull(findPet, "can't find pet with id " + pet.getId());
		
	}
	
	@Ignore
	public void shouldGetPetAll() throws Exception {
		ArrayList<Pet> pets = new ArrayList<Pet>();		
		
		pets = petService.getPetAll();
		Assert.isNull(pets, "can't find pets");
	}

	@Ignore
	public void shouldUpdatePet() throws Exception {
		ArrayList<String> photos = new ArrayList<String>();
		
		Pet pet = new Pet();
		pet.setId(1L);
		pet.setName("Andres");
		pet.setPhotoUrls(photos);		
		pet.setCategory(new Category());
		pet.getCategory().setId(1L);
		pet.getCategory().setName("Perra");		
		
		Pet petNew = petService.updatePet(pet);
		assertEquals(petNew.getId(), pet.getId());
	}

	
	
	
}
